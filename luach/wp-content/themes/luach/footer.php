<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->
		<footer id="footer" class="content">
	<div class="footer-main container-center">
		<div class="col-row">

			<div class="one-third">
				<div class="logo">
					<a href="http://cgcolors.com/dev/luach/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo/footer-blue.png" alt="Logo Estimation" /></a>			
				</div><!-- end logo -->
				<?php

	$the_query = new WP_Query( 'page_id=126' );
	while ( $the_query->have_posts() ) :
	$the_query->the_post();
    
			 echo the_content(); 	
				 endwhile;  
			 // reset post data (important!)
    wp_reset_postdata(); ?>
				<ul class="social-links-square-2 cf">								
					<li title="Facebook"><a href="https://www.facebook.com/LuachHatzibur" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li title="Twitter"><a href="https://twitter.com/luachhatzibur" target="_blank"><i class="fa fa-twitter"></i></a></li>
					
				</ul>				
			</div><!-- end  -->
<?php

	$the_query = new WP_Query( 'page_id=111' );
	while ( $the_query->have_posts() ) :
	$the_query->the_post();
    
?>	
			<div class="contact-info one-third cf">
				<?php echo the_content(); ?>

			</div><!-- end  -->		
			<?php endwhile;  
			 // reset post data (important!)
    wp_reset_postdata(); ?>
			<!---<div class="one-fourth">
				<h4>Latest Blog Posts</h4>				
				<ul class="latest-blog-posts">
					<li class="cf">
						<div class="blog-icon">
							<i class="fa fa-film"></i>
						</div>
						<div class="post-details">
							<h5><a href="#">Wheel of Time</a></h5>
							<span class="post-date">August 18, 2013</span>							
						</div>
					</li>
					<li class="cf">
						<div class="blog-icon">
							<i class="fa fa-picture-o"></i>
						</div>
						<div class="post-details">
							<h5><a href="#">The Soulforged</a></h5>
							<span class="post-date">August 18, 2013</span>						
						</div>
					</li>
					<li class="cf">
						<div class="blog-icon">
							<i class="fa fa-picture-o"></i>
						</div>
						<div class="post-details">
							<h5><a href="#">Mordred's Song</a></h5>
							<span class="post-date">August 18, 2013</span>							
						</div>
					</li>			
				</ul>
										
			</div>-- end  -->		 	
			
			<div class="one-fourth">
				<h4>Contact Us</h4>
				
		<?php echo do_shortcode('[contact-form-7 id="121" title="Contact form 1"]');?>
		</div>

			</div><!-- end  -->					
			
		</div><!-- end col-row -->	

		

		
	<div class="footer-bottom container-center cf">			
			
		<div class="bottom-left">									
			<p class="copyright">Copyright &copy; 2014 Estimation. All rights reserved</p>				
		
				<div class="responsive_scroll-top">
					<a id="link"><i class="fa fa-angle-up"></i></a>
				</div><!-- end bottom-left -->		
			
		</div><!--scroll-top end here-->
		
		<div class="bottom-right">
			<nav>
				<ul id="footer-nav">
					<li><a href="?page_id=4">Home</a></li>
					<li><a class= "service" href="javascript:void(0)">Services</a></li>
					<li><a href="javascript:void(0)">Archive</a></li>
					<li><a class= "about" href="javascript:void(0)">About</a></li>
					<li><a class= "cdfooter" href="javascript:void(0)">Contact</a></li>
				</ul>
			</nav>
		</div><!-- end bottom-right-->				
			
	</div><!-- end footer-bottom -->
</footer>
	

	<?php wp_footer(); ?>
</body>
<script type="text/javascript" >
$(document).ready(function() {
$('#link').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
 });
});
</script>
</html>